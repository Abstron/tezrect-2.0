//
// Created by Monika on 12.01.2017.
//

#include "wrapper.h"
#include "src/Utils.h"
#include <string>

extern "C" {
    #include "imageprocessor.h"
};



//Declare the C-Style functions. Magic. Woooh!
extern "C" {
    int writeOut(char* filename, int width, int height, unsigned int *buffer, char* title);
};

int wrapper::writeImage(std::string filename, int width, int height, unsigned int *buffer, std::string title) {


    //TODO: The C-Linker have some serious problems here cause it seems that he can't handel templates like the one that is used for reading Image<T>
    //TODO: bring Image<T> in this section to a C-Like friendly form with using pnglib's storage classes (inspire by imageprocessor)
    //http://www.oracle.com/technetwork/articles/servers-storage-dev/mixingcandcpluspluscode-305840.html
    //https://isocpp.org/wiki/faq/mixing-c-and-cpp
    //IF this is not helpful write wrapper class in cpp with a C method for accessing Color

    /*
     * DO's:
     * - moved #endif //TEZRECT_IMAGEPROCESSOR_H to end of imageprocessor.h
     * - added overloaded function Color::getByteW with extern C
     * - changed extern to static of global variable bitMaps[] int Color.h
     */
    int i = writeOut(Utils::stringToChar(filename), width, height, buffer, Utils::stringToChar(title));


    return i;
}




