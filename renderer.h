//
// Created by Max Schiller on 13.01.17.
//

#ifndef TEZRECT_RENDERER_H
#define TEZRECT_RENDERER_H

#include <vector>
#include "Image.h"
#include "src/CommandHandler.h"
#include "src/Utils.h"

class renderer : public CommandHandler, Utils{
public:
    renderer(Image<float> &raw, int lightDirection, int height, int width, int gridSize, std::vector<std::vector<std::vector<bool>>> &charSet);

    virtual void zoomIn(int gridZoomSteps);
    virtual void zoomOut(int gridZoomSteps);

    virtual void move(int dir, int steps);

    virtual void render();



private:
    static void rescale(std::vector<float> &vec, int scale);

    static Image<unsigned int>& shadowing(Image<float> &raw, int dir, float lightDirection);




};


#endif //TEZRECT_RENDERER_H
