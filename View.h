//
// Created by Monika on 04.02.2017.
//

#ifndef TEZRECT_VIEW_H
#define TEZRECT_VIEW_H

#include "Image.h"


class View {
public:
    View();
    //void init(Image<double> &raw, int width, int height, int gridSize);

    //NEW
    void init(int HEIGHT, int WIDTH, int gridSize, Image<float> &lraw);    //set fixed view Size
    void reInit(Image<float> &raw);              //call this every times the size changes


    int getRZ(int zoom);

    int getRHeight();
    int getRWidth();

    Image<float>& getRaw(); //never use this function




};


#endif //TEZRECT_VIEW_H
