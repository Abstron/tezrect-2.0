//
// Created by Max Schiller on 12.01.2017.
//

#ifndef TEZRECT_IMAGE_H
#define TEZRECT_IMAGE_H

#include <cstring>
#include <stdexcept>


/*
 * Add error- handling and checking.
 */
template <class T>
class Image {
private:
    T *ary;
public:
    int HEIGHT;
    int WIDTH;
    bool isInit = false;

    //constructors
    Image();

    //coyp constructor
    Image(Image<T> &obj);

    //copy assignment constructor
    Image<T>& operator= (const Image<T>& obj);

    //constructor
    Image(int height, int width);

    //access ary operator
    T &operator() (int y, int x);

    //initialize obj
    void init(int height, int width);
    T *getImg();

    //destructor
    ~Image();

};


/*
 * Do nothing
 */
template <class T>
Image<T>::Image() {
}


/*
 * Construct the object and initialize container
 */
template <class T>
Image<T>::Image(int height, int width) {
    this->init(height, width);
}


/*
 * Copy Constructor
 */
template <class T>
Image<T>::Image(Image<T> &obj) {

    //test if object is not self assigned before re-init
    if (this != &obj){

        if (Image::isInit) {
            delete[] Image::ary;
        }

        this->init(obj.HEIGHT, obj.WIDTH);
        memcpy(Image::ary, obj.ary, sizeof(int) * Image::HEIGHT * Image::WIDTH);

    }


}


/*
 * Copy assignment constructor
 */
template <class T>
Image<T>& Image<T>::operator= (const Image<T>& obj){
    //test if object is not self assigned
    if (this != &obj){

        if (Image::isInit) {
            delete[] Image::ary;
        }

        this->init(obj.HEIGHT, obj.WIDTH);
        memcpy(Image::ary, obj.ary, sizeof(int) * Image::HEIGHT * Image::WIDTH);

    }
    return *this;
}

/*
 * Initialize container
 */
template <class T>
void Image<T>::init(int height, int width) {
    Image::HEIGHT = height;
    Image::WIDTH = width;
    Image::isInit = true;

    Image::ary = new T[Image::HEIGHT*Image::WIDTH];

    for (int i = 0; i < Image::HEIGHT; ++i) {
        for (int j = 0; j < Image::WIDTH; ++j) {
            Image::ary[i*Image::WIDTH+j] = 0;
        }
    }
}


/*
 * Returning a reference to the int and let the Compiler do the calculation
 */
template <class T>
T& Image<T>::operator()(int y, int x) {

    if (y > Image::HEIGHT || x > Image::WIDTH){
        throw std::out_of_range("out of range");
    }

    if (!Image::isInit){
        //TODO: Redesign this thing
        throw std::out_of_range("Not initialized");
    }

    return Image::ary[y * Image::WIDTH + x];
}

template <class T>
T* Image<T>::getImg() {
    return Image::ary;
}


/*
 * make the memory free again
 */
template <class T>
Image<T>::~Image() {
    Image::isInit = false;
    delete[] Image::ary;
}






/*
template <class T>
class Image {
private:
    T *ary;
public:
    int HEIGHT;
    int WIDTH;

    //constructors
    Image();

    //coyp constructor
    Image(Image<T> &obj);

    //copy assignment constructor
    Image<T>& operator= (const Image<T>& obj);

    //constructor
    Image(int height, int width);

    //access ary operator
    T &operator() (int y, int x);

    //initialize obj
    void init(int height, int width);
    T *getImg();

    //destructor
    ~Image();

};


/*
 * Do nothing
 *
template <class T>
Image<T>::Image() {
}


/*
 * Construct the object
 *
template <class T>
Image<T>::Image(int height, int width) {
    this->init(height, width);
}


/*
 * Copy Constructor
 *
template <class T>
Image<T>::Image(Image<T> &obj) {
    //Image::ary = new T(*obj.ary);
    Image::ary = obj.ary;
    Image::HEIGHT = obj.HEIGHT;
    Image::WIDTH = obj.WIDTH;
}


/*
 * Copy assignment constructor
 * See Rule of Thirds
 *
template <class T>
Image<T>& Image<T>::operator= (const Image<T>& obj){
    //test if object is not self assigned
    if (this != &obj){
        //delete[] Image::ary;
        //Image::ary = new T(*obj.ary);
        Image::ary = obj.ary;
        Image::HEIGHT = obj.HEIGHT;
        Image::WIDTH = obj.WIDTH;
    }
    return *this;
}


/*
 * Initialize container
 *
template <class T>
void Image<T>::init(int height, int width) {
    Image::HEIGHT = height;
    Image::WIDTH = width;


    //TODO: Check if sizeof could be useful here
    Image::ary = new T[Image::HEIGHT*Image::WIDTH];

    for (int i = 0; i < Image::HEIGHT; ++i) {
        for (int j = 0; j < Image::WIDTH; ++j) {
            Image::ary[i*Image::WIDTH+j] = 0;
        }
    }
}


/*
 * Returning a reference to the int and let the Compiler do the calculation
 *
template <class T>
T& Image<T>::operator()(int y, int x) {
    return Image::ary[y*Image::WIDTH+x];
}


template <class T>
T* Image<T>::getImg() {
    return Image::ary;
}


/*
 * make the memory free again
 *
template <class T>
Image<T>::~Image() {
    delete [] Image::ary;
}

*/


#endif //TEZRECT_IMAGE_H
