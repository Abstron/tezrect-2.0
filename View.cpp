//
// Created by Monika on 04.02.2017.
//

#include "View.h"


/*
 * Naming is crappy. Find a way how to use standard and simple names
 * take a research in extern or virtual(?)
 */
static Image<float> raw;
static int HEIGHT;
static int WIDTH;
static int gridSize;

double rHEIGHT;
double rWIDTH;
double rGridSize;


View::View() {
}



void View::init(int height, int width, int gridSize, Image<float> &raw) {
    ::WIDTH = width;
    ::HEIGHT = height;
    ::gridSize = gridSize;
    ::raw = raw;
}

void View::reInit(Image<float> &raw) {
    ::raw = raw;
    ::rWIDTH = ::raw.WIDTH;
    ::rHEIGHT = ::raw.HEIGHT;
    ::rGridSize = ::rWIDTH / (::WIDTH / ::gridSize);
}


int View::getRZ(int zoom) {
    return  ::rGridSize * zoom;
}


int View::getRHeight() {
    return ::rHEIGHT;
}


int View::getRWidth() {
    return ::rWIDTH;
}


Image<float>& View::getRaw() {
    return raw;
}