//
// Created by Monika on 20.02.2017.
//

#include "ColorWrapper.h"
#include "Color.h"


extern "C" {
    unsigned char getByte(unsigned int color, int index);
};

/**
 * Wrapping Color.h for extracting a single byte (u. char)
 */
unsigned char ColorWrapper::getByte(unsigned int color, int index) {
    return Color::getByte<unsigned char>(color, index);
}