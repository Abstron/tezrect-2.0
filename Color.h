//
// Created by Max Schiller on 12.01.17.
//

#ifndef TEZRECT_COLOR_H
#define TEZRECT_COLOR_H

#include <iostream>


extern unsigned int bitMasks[4];

class Color {
public:

    template <typename T>
    static T getByte(unsigned int color, int index);

    template <typename T>
    static unsigned int setByte(unsigned int color, T byte, int index);
};




/*
 * Defines used Space for Integer Type
 * Integer Typically = 32 Bit = 4 Byte
 * RED = First Byte full of 1's
 * GREEN = Second Byte full of 1's ...
 * Bitwise AND Color with the Mask to filter out just what is needed
 * and move the rest x bits
 *
 *
 * 255 255   255  255     Dez.
 * 8   8     8    8       Bit's
 * 0   1     2    3       ID       <-
 * RED|GREEN|BLUE|ALPHA = INT = RGBA
 */


//Resources
//http://stackoverflow.com/questions/18591924/how-to-use-bitmask
//https://en.wikipedia.org/wiki/Mask_(computing)
//http://codeforces.com/blog/entry/18169
//http://www.vipan.com/htdocs/bitwisehelp.html


/*

const unsigned int RED = 0xFF000000;

const unsigned int GREEN = 0xFF0000;

const unsigned int BLUE = 0xFF00;

const unsigned int ALPHA = 0xFF;

unsigned int bitMasks[4] = {RED, GREEN, BLUE, ALPHA};
*/

template <typename T>
T Color::getByte(unsigned int color, int index) {

    return (color & bitMasks[index]) >> (3 - index) * 8;

}


template <typename T>
unsigned int Color::setByte(unsigned int color, T byte, int index) {

    color &= ~bitMasks[index];
    color += byte << (3 - index) * 8;
    return color;

}


#endif //TEZRECT_COLOR_H
