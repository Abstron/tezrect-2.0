//
// Created by Monika on 12.01.2017.
//

#ifndef TEZRECT_IMAGE_H
#define TEZRECT_IMAGE_H


/*
 * This class seems legit to me. Otherwise contact 00:80:41:ae:fd:7e with protocol type gopher
 */
template <class T>
class Image {
private:
    T *ary;
public:
    int HEIGHT;
    int WIDTH;
    Image();
    Image(const Image<T> &obj);
    Image(int height, int width);
    ~Image();
    T &operator() (int y, int x);
    void init(int height, int width);
    T *getImg();

};


template <class T>
Image<T>::Image() {

}

template <class T>
Image<T>::Image(int height, int width) {
    this->init(height, width);              //this does not even work. Check it
    /*Image::HEIGHT = height;
    Image::WIDTH = width;

    Image::ary = new T[Image::HEIGHT*Image::WIDTH];

    for (int i = 0; i < Image::HEIGHT; ++i) {
        for (int j = 0; j < Image::WIDTH; ++j) {
            Image::ary[i*Image::WIDTH+j] = 0;
        }
    }*/
}


template <class T>
Image<T>::Image(const Image<T> &obj) {
    ::ary = obj.ary;
}

/*
 * This is cin every case extremely bad but I can't figure out
 * how to call its own constructor an an propper way
 */
template <class T>
void Image<T>::init(int height, int width) {
    Image::HEIGHT = height;
    Image::WIDTH = width;

    Image::ary = new T[Image::HEIGHT*Image::WIDTH];

    for (int i = 0; i < Image::HEIGHT; ++i) {
        for (int j = 0; j < Image::WIDTH; ++j) {
            Image::ary[i*Image::WIDTH+j] = 0;
        }
    }
}


/*
 * Returning a reference to the int and let the Compiler do the calculation
 */
template <class T>
T& Image<T>::operator()(int y, int x) {
    //return ::ary[y*::HEIGHT+x];
    //Rethought this formula cause range of n is from 0 to n-1(!) not from 1 to n. Anyway out of bounce was a problem
    return Image::ary[y*Image::WIDTH+x];
}

template <class T>
T* Image<T>::getImg() {
    return Image::ary;
}


/*
 * make the memory free again
 */
template <class T>
Image<T>::~Image() {
    delete [] Image::ary;
}




#endif //TEZRECT_IMAGE_H
