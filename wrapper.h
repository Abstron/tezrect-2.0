#ifndef TEZRECT_WRAPPER_H
#define TEZRECT_WRAPPER_H

#include <string>


class wrapper {
public:
    static int writeImage(std::string filename, int width, int height, unsigned int *buffer, std::string title);
};


#endif //TEZRECT_WRAPPER_H
