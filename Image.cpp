//
// Created by Monika on 12.01.2017.
//

//#include "Image.h"
#include <iostream>



/* Array Stuff
 * http://stackoverflow.com/questions/936687/how-do-i-declare-a-2d-array-in-c-using-new Down to Kevin Loney's Answer
 * http://www.geeksforgeeks.org/dynamically-allocate-2d-array-c/
 */

/* Overloading Stuff
 * http://www.learncpp.com/cpp-tutorial/98-overloading-the-subscript-operator/
 */

template <class T>
Image<T>::Image(int height, int width) {
    Image::HEIGHT = height;
    Image::WIDTH = width;

    Image::ary = new T[Image::HEIGHT*Image::WIDTH];

    for (int i = 0; i < Image::HEIGHT; ++i) {
        for (int j = 0; j < Image::WIDTH; ++j) {
            Image::ary[i*Image::WIDTH+j] = 0xFFFFFFFF;
        }
    }
}


/*
 * Returning a reference to the int and let the Compiler do the calculation
 */
template <class T>
T& Image<T>::operator()(int y, int x) {
    //return ::ary[y*::HEIGHT+x];
    //Rethought this formula cause range of n is from 0 to n-1(!) not from 1 to n. Anyway out of bounce was a problem
    return Image::ary[y*Image::WIDTH+x];
}

template <class T>
T* Image<T>::getImg() {
    return Image::ary;
}


/*
 * make the memory free again
 */
template <class T>
Image<T>::~Image() {
    delete [] Image::ary;
}