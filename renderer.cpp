//
// Created by Max Schiller on 13.01.17.
//

#ifndef M_PI
#define M_PI    3.14159265358979323846f
#endif

#include "renderer.h"
#include "Image.h"
#include "src/Utils.h"
#include "Color.h"
#include "wrapper.h"
#include "View.h"
#include <vector>
#include <math.h>



/*
 * Shadow: tan(90-70)*((tan(70)*10)-26)
 */
int HEIGHT;
int WIDTH;
Image<float> raw;  //this is also shit I think
View view;          //only this sould be used globally
int viewY = 0;
int viewX = 0;
int gridSize;
std::vector<std::vector<std::vector<bool>>> charSet;
int realGridSize;

renderer::renderer(Image<float> &raw, int lightDirection, int height, int width, int gridSize, std::vector<std::vector<std::vector<bool>>> &charSet) {
    ::raw = raw;
    ::HEIGHT = height;
    ::WIDTH = width;
    ::gridSize = gridSize;
    ::charSet = charSet;


    ::viewY = 0;
    ::viewX = 0;
    view.init(::HEIGHT, ::WIDTH, ::gridSize, ::raw);



    Image<float> local;
    local.init((::raw.WIDTH / (double)::WIDTH) * ::HEIGHT, ::raw.WIDTH);

                            //todo: this is shittr
    for (int y = ::viewY; y < (::raw.WIDTH / ::WIDTH) * ::HEIGHT; ++y) {
        for (int x = ::viewX; x < ::raw.WIDTH; ++x) {

            if (y < ::raw.HEIGHT || x < ::raw.WIDTH){
                local(y,x) = ::raw(y,x);
            } else {
                local(y,x) = 0;
            }

        }
    }

    ::view.reInit(local);
}



void renderer::zoomIn(int gridZoomSteps) {


    int spacing = ::view.getRZ(gridZoomSteps);

    //TODO: refactor if down calculations could be replaced by this
    //get real y coord on view
    //::viewY = ::viewY + spacing;
    //::viewX = ::viewX + spacing;


    Image<float> local;
    local.init(::view.getRHeight() - (spacing * 2), ::view.getRWidth() - (spacing * 2));



    for (int y = ::viewY + spacing; y < ::viewY + ::view.getRHeight() - spacing; ++y) {
        for (int x = ::viewX + spacing; x < ::viewX + ::view.getRWidth() - spacing; ++x) {
            if (y < ::raw.HEIGHT || x < ::raw.WIDTH){
                local(y - ::viewY - spacing, x - ::viewX - spacing) = ::raw(y,x);
            } else {
                local(y - ::viewY + spacing, x - viewX + spacing) = 0;
            }
        }
    }

    ::view.reInit(local);

    ::viewY = ::viewY + spacing;
    ::viewX = ::viewX + spacing;

}



void renderer::zoomOut(int gridZoomSteps) {

    int spacing = ::view.getRZ(gridZoomSteps);


    //TODO:
    //get real y coord on view
    //::viewY = ::viewY + spacing;
    //::viewX = ::viewX + spacing;


    Image<float> local;
    local.init(::view.getRHeight() + (spacing * 2), ::view.getRWidth() + (spacing * 2));



    for (int y = ::viewY - spacing; y < ::viewY + ::view.getRHeight() + spacing; ++y) {
        for (int x = ::viewX - spacing; x < ::viewX + ::view.getRWidth() + spacing; ++x) {
            if (y < ::raw.HEIGHT || x < ::raw.WIDTH){
                local(y - ::viewY + spacing, x - ::viewX + spacing) = ::raw(y,x);
            } else {
                local(y - ::viewY + spacing, x - viewX + spacing) = 0;
            }
        }
    }

    ::view.reInit(local);

    ::viewY = ::viewY - spacing;
    ::viewX = ::viewX - spacing;
}


void renderer::move(int dir, int lsteps) {

    int steps = ::view.getRZ(lsteps);
    Image<float> local;
    local.init(::view.getRHeight(), ::view.getRWidth());



    //TODO: Check this out if its fixed!


    switch(dir){
        case 1:
            for (int y = ::viewY - steps; y < ::viewY + ::view.getRHeight() - steps; ++y) {
                for (int x = viewX; x < ::viewX + ::view.getRWidth(); ++x) {
                    if (y < ::raw.HEIGHT || x < ::raw.WIDTH){
                        local(y - ::viewY, x - ::viewX) = ::raw(y,x);
                    } else {
                        local(y - ::viewY, x - viewX) = 0;
                    }
                }
            }
            ::viewY = ::viewY - steps;
            break;

        case 2:
            for (int y = ::viewY; y < ::viewY + ::view.getRHeight(); ++y) {
                for (int x = viewX + steps; x < ::viewX + ::view.getRWidth() + steps; ++x) {
                    if (y < ::raw.HEIGHT || x < ::raw.WIDTH){
                        local(y - ::viewY, x - ::viewX) = ::raw(y,x);
                    } else {
                        local(y - ::viewY, x - viewX) = 0;
                    }
                }
            }
            ::viewX = ::viewX + steps;
            break;

        case 3:
            for (int y = ::viewY + steps; y < ::viewY + ::view.getRHeight() + steps; ++y) {
                for (int x = viewX; x < ::viewX + ::view.getRWidth(); ++x) {
                    if (y < ::raw.HEIGHT || x < ::raw.WIDTH){
                        local(y - ::viewY, x - ::viewX) = ::raw(y,x);
                    } else {
                        local(y - ::viewY, x - viewX) = 0;
                    }
                }
            }
            ::viewY = ::viewY + steps;
            break;

        case 4:
            for (int y = ::viewY; y < ::viewY + ::view.getRHeight(); ++y) {
                for (int x = viewX - steps; x < ::viewX + ::view.getRWidth() - steps; ++x) {
                    if (y < ::raw.HEIGHT || x < ::raw.WIDTH){
                        local(y - ::viewY, x - ::viewX) = ::raw(y,x);
                    } else {
                        local(y - ::viewY, x - viewX) = 0;
                    }
                }
            }
            ::viewX = ::viewX - steps;
            break;

    }

    ::view.reInit(local);

}


void renderer::rescale(std::vector<float> &vec, int scale){

    float part = vec.size()/(float)scale;
    std::vector<float> newVec;

    for (float i = 0; i < vec.size(); i = i + part) {

        int first = (int)i;
        float second = Utils::getPostFloat(i);

        //(A+(B*d))/(1+d)   d=digit part (0.XX)         //TODO: Test if normal Mittelwert-Formel is better
        float res = (vec[first] + (vec[first +1] * second)) / (1 + second);
        newVec.push_back(res);


    }
    vec = newVec;
}


//TODO: Refactor this. Could possibly
Image<unsigned int>& renderer::shadowing(Image<float> &raw, int dir, float lightIncidence = 45) {

    Image<unsigned int> ren(raw.HEIGHT, raw.WIDTH);

    //Get the Neighbour Cell in dependence of direction
    //Hardcoded Mask would also be possible but a formula is much more fancyyy
    //int moveY[] = { -1, 0, 1, 1, 1, 0, -1, -1};
    //int moveX[] = { -1, -1, -1, 0, 1, 1, 1, 0};

    //for PI see: https://msdn.microsoft.com/de-de/library/4hwaceh6.aspx
    int maskY = (int)cos((2 * M_PI * (dir - 1)) / 4);
    int maskX = (int)cos((2 * M_PI * (dir - 2)) / 4);

    for (int y = 0; y < ::HEIGHT; ++y) {
        for (int x = 0; x < ::WIDTH; ++x) {




            /*
             * FORMULA: tan(90-BETA)*((tan(BETA)*a)-i = 2D Shadowing
             *
             * P1 = raw(y,x)
             * P2 = raw(y + maskY, x + maskX)
             */



            if (y + maskY < HEIGHT or y + maskY > 0 or x + maskX < WIDTH or x + maskX > 0){


                //TODO: Find out where to set (float)
                float shadowHeight = raw(y + maskY, x + maskX);
                //TODO: Insert Case anywhere else cause of wrong typecasting
                shadowHeight = shadowHeight - (float)tan(90 - lightIncidence) * ((tan(lightIncidence) * raw(y,x)) - 1);

                //if sH < 0 = darker, sH > 0 brighter



                /*
                 * Note: 000 is dark and 255 is bright
                 *
                 * Set a max and min value and every heights above will be completely dark or white
                 * Inside the scope the will be unique
                 */



                int max = 16;

                //Numbers which are bigger than 10 or -10 will be set to 10 or -10
                if (shadowHeight > max){
                    shadowHeight = max;
                } else if (shadowHeight < max * -1){
                    shadowHeight = max * -1;
                }

                //move negative space to positive
                //shadowHeight += max;

                //TODO Crap(shadowHeight / max) * 128;
                //(128/max) * 8
                int byte = (128 / max) * (int)shadowHeight;

                //move negative space to positive
                byte += 128;



                unsigned int color = 0;
                for (int i = 0; i < 3; ++i) {
                    color = Color::setByte(color, (char)byte, i);
                }



                ren(y,x) = color;


            }

            /*
             *if (P2 < 0 || P1 > HEIGHT OR WIDTH)
             *
             *  CALC P2shadowHeight
             *
             *  if (P2shadowHeight > P2Height)
             *
             *      saturation = P2shadowHeight - P2Height;
             *
             *   else
             *
             *      saturation = P2shadowHeight - P2Height
             */

        }
    }

    return ren;
    
    
}

void renderer::render() {

    //new LocalImage


    //rescale view to HEIGHT & WIDTH
    //read out every y line and rescale and add line to new local image
    //read out every x line and rescale

    Image<float> rawImg = ::view.getRaw(); //TODO: get it really with its ary?


    Image<float> scaledImg;


    //
    scaledImg.init(rawImg.HEIGHT, rawImg.WIDTH);



    std::vector<float> local;

    /*
     * scaling in horizontal line
     */

    for (int y = 0; y < rawImg.HEIGHT; ++y) {
        for (int x = 0; x < rawImg.WIDTH; ++x) {
            local.push_back(rawImg(y,x));
        }
        rescale(local, ::WIDTH);
        for (int x = 0; x < ::WIDTH; ++x) {
            scaledImg(y,x) = local[x];
        }
        local.clear();
    }


    //vertical scaling
    for (int x = 0; x < rawImg.WIDTH; ++x) {
        for (int y = 0; y < rawImg.HEIGHT; ++y) {
            local.push_back(rawImg(y,x));
        }
        rescale(local, ::HEIGHT);
        for (int y = 0; y < ::HEIGHT; ++y) {
            scaledImg(y,x) = local[y];
        }
    }




    //TODO: Debug the whole shadowing process
    //Rendering
    Image<unsigned int> img = shadowing(scaledImg, 1, 45.0);





    //Image + Coords & Grid
    //calc realGridSize

    //writing out LocalImage
    wrapper::writeImage("./charSetprint.png", WIDTH, HEIGHT, img.getImg(), "title");
}





    /*int rulerSize = 10;

    //TODO: Add Space for ruler here?
    Image img(height + rulerSize, width + rulerSize);


    int hCount = 0;
    int vCount = 0;


    //initialize Ruler and Grid
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {



            //TODO: Rewrite this in an mathematically way



            //Horizontal
            if (y < rulerSize){

                //setPixel = background

                if (y == 8){
                    if (Utils::getPostFloat(x / (float)gridSize) == 0){


                        for (int yinner = 0; yinner < 7; ++yinner) {
                            for (int xinner = 0; xinner < 5; ++xinner) {
                                //img(y + yinner,x + xinner) =
                            }
                        }
                    }
                }
            }


            /*
             * gridSize how to find out where Lines / Numbers to set
             *
             * ID(y or x) / gridSie 0 If its an full (int) number without floating points set
             *
             *

    

        }
    }*/

    /*Image<unsigned int> img(260, 10);
    int charID = 0;

    for (int y = 0; y < 260; ++y) {
        for (int x = 0; x < 10; ++x) {

            //TODO: Manually setting Background Color here is giving shitty output image. Just a view Red Pix.
            //img(y, x) = 0xFFFFFFFF;


            //               % x is the distance
            if (x == 2 && (y % 10) == 0 && charID < charSet.size()){

                for (int yInner = 0; yInner < 7; ++yInner) {
                    for (int xInner = 0; xInner < 5; ++xInner) {


                        if (charSet[charID][yInner][xInner]){
                            img(y + yInner, x + xInner) = 0xFF000000;
                        }




                    }
                }

                charID++;
            }
        }
    }

    img(0,9) = 0xFFFFFFFF;
    img(0,9) = 0xFF000000;
    img(1,9) = 0x00FF0000;
    img(2,9) = 0x0000FF00;
    img(3,9) = 0x9F03FF00;



    wrapper::writeImage("./charSetprint.png", 10, 260, img.getImg(), "title");*/





    //




