//
// Created by Max Schiller on 19.04.17.
//

#ifndef TEZRECT_HASH_H
#define TEZRECT_HASH_H


class Hash {
public:
    static constexpr unsigned int getHash(const char* str, int h = 0) {

        return !str[h] ? 5381 : (Hash::getHash(str, h+1) * 33) ^ str[h];

    }
};


#endif //TEZRECT_HASH_H
