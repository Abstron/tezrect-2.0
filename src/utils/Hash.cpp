//
// Created by Max Schiller on 19.04.17.
// Inspired by http://stackoverflow.com/questions/8317508/hash-function-for-a-string
//


#include "Hash.h"


/*
 * Do research why you cant call this function when both files (header AND source) are used
 */
constexpr unsigned int Hash::getHash(const char* str, int h) {

    return !str[h] ? 5381 : (Hash::getHash(str, h+1) * 33) ^ str[h];

}
