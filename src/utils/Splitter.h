//
// Created by Max Schiller on 21.04.17.
// inspired by http://stackoverflow.com/questions/236129/split-a-string-in-c
//

#ifndef TEZRECT_SPLITTER_H
#define TEZRECT_SPLITTER_H


#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <iterator>




class Splitter{
public:

    /*
     * Build in empty check for strings
     */
    template<typename Out>
    static void split(const std::string &s, char delim, Out result) {
        std::stringstream ss;
        ss.str(s);
        std::string item;
        while (std::getline(ss, item, delim)) {
            *(result++) = item;
        }
    }


   static std::vector<std::string> split(const std::string &s, char delim) {
        std::vector<std::string> elems;
        split(s, delim, std::back_inserter(elems));
        return elems;
    }





};


#endif //TEZRECT_SPLITTER_H
