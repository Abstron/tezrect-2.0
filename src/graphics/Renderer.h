//
// Created by Monika on 17.04.2017.
//

#ifndef TEZRECT_RENDERER_H
#define TEZRECT_RENDERER_H

#include "../DataHandler.h"


class Renderer {
public:
    int val;
    DataHandler *dh;


    Renderer(DataHandler *dh);
    void init();

    void zoomIn();

    void render();
};


#endif //TEZRECT_RENDERER_H
