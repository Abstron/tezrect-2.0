//
// Created by Max Schiller on 21.04.17.
//

#include "DataHandler.h"
#include "io/FileReader.h"



DataHandler::DataHandler() {

}



/*
 * Sets file names
 */
void DataHandler::init(std::string fontFile, std::string dataFile) {

    this->fontFilename = fontFile;
    this->dataFilename = dataFile;

    //TODO: Check if existing

}


/*
 * Reads data- and fontfiles
 */
void DataHandler::load() {

    this->fontData = FileReader::fontReader(this->fontFilename);
    this->rawData = FileReader::dataReader(this->dataFilename);


}