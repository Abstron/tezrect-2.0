//
// Created by Max Schiller on 21.04.17.
//

#ifndef TEZRECT_DATAHANDLER_H
#define TEZRECT_DATAHANDLER_H


#include <string>
#include <vector>
#include <memory>

#include "Image.h"

class DataHandler {
public:

    std::string fontFilename;
    std::string dataFilename;

    std::vector<std::vector<std::vector<bool>>> fontData;
    std::unique_ptr<Image<float>> rawData;


    //func's
    DataHandler();
    void init(std::string fontFile, std::string dataFile);
    void load();
    void getFont();

    Image<int>* getData();

};


#endif //TEZRECT_DATAHANDLER_H
