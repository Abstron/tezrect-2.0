//
// Created by Max Schiller on 06.04.17.
//

#ifndef TEZRECT_COMMANDHANDLER_H
#define TEZRECT_COMMANDHANDLER_H

#include "graphics/Renderer.h"
#include <iostream>



class CommandHandler {
private:
    Renderer *ren;

public:
    CommandHandler(Renderer &renderer);

    void commandListener();
    void process(std::string command);



};


#endif //TEZRECT_COMMANDHANDLER_H
