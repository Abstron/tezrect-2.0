//
// Created by Max Schiller on 12.01.17.
//

#include "Utils.h"
#include <string>
#include <string.h>
#include <sstream>
#include <fstream>
#include <limits>



constexpr unsigned int getHash(const char* str, int h = 0) {
    return !str[h] ? 5381 : (getHash(str, h+1) * 33) ^ str[h];
}


template<typename Out>
void Utils::split(const std::string &s, char delim, Out result) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

std::vector<std::string> Utils::split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}







char* Utils::stringToChar(std::string str) {

    char *cstg = new char[str.length() + 1];
    strcpy(cstg, str.c_str());


    return cstg;
}

/*
 * See external Docs: https://s-media-cache-ak0.pinimg.com/236x/20/49/a3/2049a3d6db7b3b03d18457889f7c2cab.jpg
 */
std::vector<std::string> Utils::splitter(std::string line, char split) {

    std::stringstream ss;
    ss << line;


    std::string segment;
    std::vector<std::string> seglist;

    while(std::getline(ss, segment, split)) {
        seglist.push_back(segment);
    }


    return seglist;

}


/*
 * Senseless, Huh?
 */
int Utils::getPreFloat(float f) { return (int)f; }


int Utils::getPostFloat(float f) {

    int local = (int)f;
    f = f - local;

    return f;
}


/*
 * Test me
 */
std::ifstream& Utils::gotoLine(std::ifstream &file, int num) {
    file.seekg(std::ios::beg);
    for(int i=0; i < num - 1; ++i){
        file.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }

    return file;

}