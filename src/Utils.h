//
// Created by Max Schiller on 12.01.17.
//

#ifndef TEZRECT_UTILS_H
#define TEZRECT_UTILS_H

#include <string>
#include <iostream>
#include <vector>
#include <fstream>


class Utils {
public:
    static unsigned int getHash(const char*, int);


    //Refactor

    template<typename Out>
    static void split(const std::string &s, char delim, Out result);
    std::vector<std::string> split(const std::string &s, char delim);

    static char* stringToChar(std::string);
    static std::vector<std::string> splitter(std::string, char);
    static int getPreFloat(float);
    static int getPostFloat(float);
    static int resizeVec(std::vector<bool>); //TODO: Write recursive resize function for n parameters with n depth
    static std::ifstream& gotoLine(std::ifstream&, int);

};


#endif //TEZRECT_UTILS_H
