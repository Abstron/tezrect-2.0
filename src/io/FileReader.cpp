//
// Created by Max Schiller on 13.01.17.
//

#include "FileReader.h"
#include <string>
#include "../../src/Image.h"
#include "../utils/Splitter.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>

/*
 * Deprecated(?)
 */
std::string FileReader::dataParser(std::string strg) {
    std::vector<std::string> vstrg =  Splitter::split(strg, ' ');
    return vstrg[1];
}


std::unique_ptr<Image<float>> FileReader::dataReader(std::string filename) {

    //Try Open File
    std::ifstream datafile(filename, std::ios::in);
    int height;
    int width;


    std::unique_ptr<Image<float>> raw(new Image<float>());



    //test file
    //TODO: add error handling
    if (datafile.is_open()) {




        float local;
        // could be replaced with (X ; X ; row++) and remove the increments in if-statements
        // but gives me and obvious warnings of not reachable code in CLion
        // Probably see this: http://stackoverflow.com/questions/27959348/unreachable-code-in-for-loop-increment Answered by himself cause of, ..getting up-votes?
        int row = 0;
        for(std::string datarow; std::getline(datafile, datarow); ) {

            std::istringstream in(datarow);

            //set height
            if (row == 0){
                height = std::stoi(dataParser(datarow));
                row++;
                continue;
            }

            //set width
            if (row == 1){
                width = std::stoi(dataParser(datarow));
                row++;
                //init Images as size was red
                raw->init(height, width);
                continue;
            }

            //jump the rest of lines
            if(row < 6){
                row++;
                continue;
            }


            //iterate threw columns
            for (int column = 0; column < width; ++column) {

                in >> local;
                raw->operator()(row - 6, column) = local;

            }

            row++;

        }

        return raw;

    } else {
        std::cout << "could not open the file!" << std::endl;
    }



}



std::vector<std::vector<std::vector<bool>>> FileReader::fontReader(const std::string filename) {

    std::string row;
    std::ifstream fontfile(filename, std::ios::in);
    std::vector<std::vector<std::vector<bool>>> charList;

    //dirty resize with expected data. See Utils.h
    //using resize cause that much push_backs for a temporary solution would be an overkill.
    //Imagine this storrage as an 3D Matrix. One layer is separating the characters and the than just 2D Layers are needed
    charList.resize(27);
    for (int z = 0; z < 27; ++z) {
        charList[z].resize(7);
        for (int y = 0; y < 7; ++y) {
            charList[z][y].resize(5);
            //for (int x = 0; x < 5; ++x) {}
        }
    }



    if (fontfile.is_open()){


        int charID = 0;
        int rowID = 0;

        //Read a single Line
        while (getline (fontfile, row)){


            if (row.empty()){
                continue;
            }

            //vector of chars via using the constructor
            std::vector<char> dataRow(row.begin(), row.end());


            //TODO: Remove this
            if (dataRow[0] == '*'){
                std::cout << "end";
            }


            if (dataRow[0] == '#'){
                continue;
            }

            if (dataRow[0] == '-'){
                rowID = 0;
                charID++;
                //continue because a new character begins
                continue;
            }



            //isn't that long
            int len = dataRow.size();
            //iterate trow row and switch cases
            for (int column = 0; column < len; ++column) {

                switch (dataRow[column]){
                    case '0':
                        charList[charID][rowID][column] = false;
                        break;
                    case '1':
                        charList[charID][rowID][column] = true;
                        break;
                }

            }


            rowID++;

        }

        fontfile.close();
    } else {
        std::cout << "could not open the file!" << std::endl;
    }


    return charList;
}