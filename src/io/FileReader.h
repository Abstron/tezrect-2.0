//
// Created by Max Schiller on 13.01.17.
//

#ifndef TEZRECT_FILEREADER_H
#define TEZRECT_FILEREADER_H

#include <string>
#include <vector>
#include <memory>
#include "../../src/Image.h"


class FileReader {
public:
    //static Image<float> *raw;

    static std::string dataParser(std::string);
    static std::unique_ptr<Image<float>> dataReader(const std::string);
    static std::vector<std::vector<std::vector<bool>>> fontReader(const std::string);

};


#endif //TEZRECT_FILEREADER_H
