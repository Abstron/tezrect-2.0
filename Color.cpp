//
// Created by Max Schiller on 12.01.17.
//


#include "Color.h"
#include <iostream>


/*
 * Defines used Space for Integer Type
 * Integer Typically = 32 Bit = 4 Byte
 * RED = First Byte full of 1's
 * GREEN = Second Byte full of 1's ...
 * Bitwise AND Color with the Mask to filter out just what is needed
 * and move the rest x bits
 *
 *
 * 255 255   255  255     Dez.
 * 8   8     8    8       Bit's
 * 0   1     2    3       ID       <-
 * RED|GREEN|BLUE|ALPHA = INT = RGBA
 */


//Resources
//http://stackoverflow.com/questions/18591924/how-to-use-bitmask
//https://en.wikipedia.org/wiki/Mask_(computing)
//http://codeforces.com/blog/entry/18169
//http://www.vipan.com/htdocs/bitwisehelp.html



const unsigned int RED = 0xFF000000;

const unsigned int GREEN = 0xFF0000;

const unsigned int BLUE = 0xFF00;

const unsigned int ALPHA = 0xFF;

unsigned int bitMasks[4] = {RED, GREEN, BLUE, ALPHA};

/*
int Color::getByte(unsigned int color, int index) {
    switch (index){
        case 0 :
            return (color & RED) >> 24;
        case 1 :
            return (color & GREEN) >> 16;
        case 2 :
            return (color & BLUE) >> 8;
        case 3 :
            return color & ALPHA;
    }
}

unsigned int Color::setByte(unsigned int color, unsigned int byte, int index) {
    switch (index){
        case 0 :
            color &= ~RED;
            color += byte << 24;
            return color;
        case 1 :
            color &= ~GREEN;
            color += byte << 16;
            return color;
        case 2 :
            color &= ~BLUE;
            color += byte << 8;
            return color;
        case 3 :
            color &= ~ALPHA;
            color += byte;
            return color;
    }
}
*/