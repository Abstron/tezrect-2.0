//
// Created by Monika on 20.02.2017.
//

#ifndef TEZRECT_COLORWRAPPER_H
#define TEZRECT_COLORWRAPPER_H


class ColorWrapper {
public:
    static unsigned char getByte(unsigned int color, int index);
};

#endif //TEZRECT_COLORWRAPPER_H
